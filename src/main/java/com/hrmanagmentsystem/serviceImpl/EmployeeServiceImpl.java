package com.hrmanagmentsystem.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hrmanagmentsystem.bean.EmployeeDetails;
import com.hrmanagmentsystem.bean.LoginDetails;
import com.hrmanagmentsystem.repository.EmployeeRepository;
import com.hrmanagmentsystem.repository.LoginDetailsRepository;
import com.hrmanagmentsystem.services.EmployeeService;

@Component
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	LoginDetailsRepository loginDetailsRepository;

	@Override
	public LoginDetails addEmployee(EmployeeDetails employee) {
		// TODO Auto-generated method stub
		employee.setStatus(1);
		EmployeeDetails emp=employeeRepository.save(employee);

		LoginDetails loginDetails=new LoginDetails();
		loginDetails.setUser_id(emp.getId());
		loginDetails.setUsername(emp.getFname().toLowerCase()+""+emp.getLname().toLowerCase());
		loginDetails.setPassword("123456");
		loginDetails.setRoll(employee.getRoll());
		loginDetails.setStatus(1);
		
		LoginDetails login=loginDetailsRepository.save(loginDetails);
		return login;
	}

}
