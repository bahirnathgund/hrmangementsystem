package com.hrmanagmentsystem.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.hrmanagmentsystem.bean.LoginDetails;
import com.hrmanagmentsystem.repository.LoginDetailsRepository;
import com.hrmanagmentsystem.services.LoginService;

@Component
public class LoginServiceImpl implements LoginService{

	@Autowired
	LoginDetailsRepository loginDetailsRepository;

	@Override
	public LoginDetails getLoginDetails(LoginDetails loginDetails) {
		// TODO Auto-generated method stub
		LoginDetails login=loginDetailsRepository.findByUsernameAndPassword(loginDetails.getUsername(), loginDetails.getPassword());

		return login;
	}

}
