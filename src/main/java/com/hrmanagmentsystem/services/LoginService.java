package com.hrmanagmentsystem.services;

import com.hrmanagmentsystem.bean.LoginDetails;

public interface LoginService {

	LoginDetails getLoginDetails(LoginDetails loginDetails);

}
