package com.hrmanagmentsystem.services;

import com.hrmanagmentsystem.bean.EmployeeDetails;
import com.hrmanagmentsystem.bean.LoginDetails;

public interface EmployeeService {

	LoginDetails addEmployee(EmployeeDetails employee);
}
