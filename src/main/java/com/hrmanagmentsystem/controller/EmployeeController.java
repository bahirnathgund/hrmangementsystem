package com.hrmanagmentsystem.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hrmanagmentsystem.bean.EmployeeDetails;
import com.hrmanagmentsystem.bean.LoginDetails;
import com.hrmanagmentsystem.constant.ResponseStatus;
import com.hrmanagmentsystem.services.EmployeeService;

@CrossOrigin(origins = "*", allowedHeaders ="*")
@RestController
@RequestMapping("")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	String location="Employee controller";

	@RequestMapping(value="/addEmployee", method=RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> addEmployee(@RequestBody EmployeeDetails EmployeeDetails)
	{

		final String success_msg="Employye Add succesfull.";
		final String failure_msg="Fail to Add employee.";
		try {
			LoginDetails login=employeeService.addEmployee(EmployeeDetails);
			if(login!=null)
			{
				return BaseController.responseDataSuccess(login,success_msg, location);
			}else
			{
				return BaseController.responseDataFailure(failure_msg, location);
			}
		}catch (Exception e) {
			// TODO: handle exceptionResponseStatus.LOG.error("Inside ScenarioComparisonController - programScenarioListCalculatuon "+ e);
			ResponseStatus.LOG.error(" "+ e);
			return BaseController.responseDataFailure(failure_msg, location);

		}

	}
/*
	@RequestMapping(value="/addEmployeeRoll", method=RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> addEmployeeRoll(@RequestBody LoginDetails loginDetails)
	{

		final String success_msg="Employye Add succesfull.";
		final String failure_msg="Fail to Add employee.";
		try {
			int result=employeeService.addEmployee(EmployeeDetails);
			if(result==1)
			{
				return BaseController.responseDataSuccess(1,success_msg, location);
			}else
			{
				return BaseController.responseDataFailure(failure_msg, location);
			}
		}catch (Exception e) {
			// TODO: handle exceptionResponseStatus.LOG.error("Inside ScenarioComparisonController - programScenarioListCalculatuon "+ e);
			ResponseStatus.LOG.error(" "+ e);
			return BaseController.responseDataFailure(failure_msg, location);

		}

	}
*/
}
