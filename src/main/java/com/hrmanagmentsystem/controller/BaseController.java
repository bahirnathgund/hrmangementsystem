package com.hrmanagmentsystem.controller;

import java.util.HashMap; 
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hrmanagmentsystem.constant.ResponseStatus;

public class BaseController {

	
	public static  ResponseEntity<Map<String, Object>> responseDataSuccess(Object objList, String msg,String location) 
	{
			Map<String, Object> json = new HashMap<String, Object>();
			HttpHeaders headers=new HttpHeaders();
			Map<String, Object> message=new HashMap<String, Object>();
			ResponseEntity<Map<String, Object>> result = null;
			    message.put("location", location);
				message.put("summary", msg);
				message.put("code", 200);
				json.put("success", true);
				json.put("message", message);
				json.put("Result", objList);
				headers.add("Content-Type", "application/json; charset=UTF-8");
				headers.add("X-Fsl-Location", "/");
				headers.add("X-Fsl-Response-Code", ResponseStatus.Success_Code);
				result=(new ResponseEntity<Map<String, Object>>(json, headers, HttpStatus.OK)); 
				return result;   
	} 
		
public static  ResponseEntity<Map<String, Object>> ResponseDataSuccessfully(Object objList_1,Object objList_2, String msg)
	{
			Map<String, Object> json = new HashMap<String, Object>();
			HttpHeaders headers=new HttpHeaders();
			Map<String, Object> message=new HashMap<String, Object>();
			ResponseEntity<Map<String, Object>> result = null;
				message.put("summary", msg);
				message.put("code", 200);
				json.put("success", true);
				json.put("message", message);
				json.put("Result", objList_1);
				json.put("Data", objList_2);
				headers.add("Content-Type", "application/json; charset=UTF-8");
				headers.add("X-Fsl-Location", "/");
				headers.add("X-Fsl-Response-Code", ResponseStatus.Success_Code);
				result=(new ResponseEntity<Map<String, Object>>(json, headers, HttpStatus.OK)); 
				return result;   
	} 
		
		
// response for import file
public static  ResponseEntity<Map<String, Object>> ImportResponseDataSuccessfully(Object pass_record,Object fail_record, String msg)
{
					Map<String, Object> json = new HashMap<String, Object>();
					HttpHeaders headers=new HttpHeaders();
					Map<String, Object> message=new HashMap<String, Object>();
					ResponseEntity<Map<String, Object>> result = null;
					message.put("summary", msg);
					message.put("code", 200);
					json.put("success", true);
					json.put("message", message);
					json.put("pass_record", pass_record);
					json.put("fail_record", fail_record);
					headers.add("Content-Type", "application/json; charset=UTF-8");
					headers.add("X-Fsl-Location", "/");
					headers.add("X-Fsl-Response-Code", ResponseStatus.Success_Code);
					result=(new ResponseEntity<Map<String, Object>>(json, headers, HttpStatus.OK)); 
					return result;   
} 
				
				
public static ResponseEntity<Map<String, Object>> responseDataFailure(String msg,String location) 
{
					Map<String, Object> json = new HashMap<String, Object>();
					HttpHeaders headers=new HttpHeaders();
					Map<String, Object> message=new HashMap<String, Object>();
					ResponseEntity<Map<String, Object>> result = null;
			 		message.put("location",location);
			 		message.put("summary",msg);
			 		message.put("code",500);
			 		json.put("success", false);
			 		json.put("message", message);
			 		headers.add("Content-Type", "application/json; charset=UTF-8");
			 		headers.add("X-Fsl-Location", "/");
				    headers.add("X-Fsl-Response-Code", ResponseStatus.Failed_Code);
				    result=(new ResponseEntity<Map<String, Object>>(json, headers, HttpStatus.OK)); 
				    return result;   
} 
		
public static  ResponseEntity<Map<String, Object>> updateResponseDataSuccessfully(String msg,String location)
{
				Map<String, Object> json = new HashMap<String, Object>();
				HttpHeaders headers=new HttpHeaders();
				Map<String, Object> message=new HashMap<String, Object>();
				ResponseEntity<Map<String, Object>> result = null;
			    message.put("location", location);
				message.put("summary", msg);
				message.put("code",200);
				json.put("success", true);
				json.put("message", message);
				headers.add("Content-Type", "application/json; charset=UTF-8");
				headers.add("X-Fsl-Location", "/");
				headers.add("X-Fsl-Response-Code", ResponseStatus.Success_Code);	
				result=(new ResponseEntity<Map<String, Object>>(json, headers, HttpStatus.OK)); 
				return result;   
}
		
public static ResponseEntity<Map<String, Object>> warningResponse(String msg,String location)
{			
			Map<String, Object> json = new HashMap<String, Object>();
			HttpHeaders headers=new HttpHeaders();
			Map<String, Object> message=new HashMap<String, Object>();
			ResponseEntity<Map<String, Object>> result = null;
			message.put("location",location);
			message.put("summary",msg);
			message.put("code",422);
			json.put("success", false);
			json.put("message", message);
			headers.add("Content-Type", "application/json; charset=UTF-8");
			headers.add("X-Fsl-Location", "/");
		    headers.add("X-Fsl-Response-Code", ResponseStatus.Failed_Code);
			result=(new ResponseEntity<Map<String, Object>>(json, headers, HttpStatus.OK)); 
			return result;   
}
		
		


}
