package com.hrmanagmentsystem.constant;

import java.time.OffsetDateTime; 
import java.time.ZoneOffset;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hrmanagmentsystem.controller.LoginController;



public class ResponseStatus {
	public static final String SUCCESS="SUCCESS";
	public static final String FAILED="FAILURE";
	public static final String DATA_NOT_FOUND="DATA NOT FOUND";
	public static final String Auth_Success= "Authenticated successfully.";
	public static final String Invalid_Credentials= "Invalid credentials. Please check the username and  password.";
	public static final String Empty_Credentials= "Username or password cannot be empty.";
	public static final String LOGOUT_Success= "User Logout successfully.";
	public static final String EmailNOTFOUNB_ERROR= "We didn't find an account for that e-mail address.";
	public static final String EmailSent_SUCCESS= "Password reset successful.";
	public static final String FAIL="FAIL";
	public static final String PASS="PASS";
	public static final String EMAIL_ALREADY_EXISTS="Email Already Exists";
	public static final String Success_Code="200(ok)";
	public static final String Failed_Code="422(Bad Request)";
	public static final String Error_Code="500(Exception)";
	public static final String fileuplod= "file upload successfully.";
	public static final String empty_file= "Empty file is not allow.";
	public static final String fileUploadFailure ="File Upload Failure";
	public static final String PROGRAM_CBS ="PROGRAM_CBS";
	public static final String PROGRAM_NUMBER_NOT_EXISTS = "Program number not exists";
	public static final String UPDATEDMSG="User updated successfully";
	public static final OffsetDateTime currentDate = OffsetDateTime.now(ZoneOffset.UTC );
	public static final String USER_CREATED_SUCCESS="User Created Success";
	public static final String USER_DELETED_SUCCESS= "User Deactivated successfully.";
	public static final String USER_ACTIVATED_SUCCESS= "User Activated successfully.";
	public static final String USER_STATUS_SUCCESS= "User Status Updated successfully.";
	public static final String PASSWORD_CHANGE_SUCCESS="Password Changed Successfully";
	public static final String PASSWORD_CHANGE_FAILED="Password Change Failed";
	public static final String INACTIVE_USER_MSG="You have not been Activated Please contact Administrator";
	public static final String COMPANY_UPDATE_FAILED= "Company Updatation FAILED.";
	//Common Response Status
	public static final String ENTER_VALID_DATA="Enter valid data.";
	public static final String MANDAT0RY_DETAILS_REQUIRED="“Mandatory details required.";
	public static final String CBS_DATA_IMPORTED_SUCCESSFULLY="CBS data imported successfully.";	
	//Company Response Status
	public static final String COMPANY_DETAILS_GET_SUCCESSFULLY="Company details get successfully.";
	public static final String RESPONSE_SUMMARY="Company details get successfully.";
	public static final String COMPANY_DETAILS_UPDATE_SUCCESSFULLY="Company Profile Has Been Updated Successfully.";
	//Segments Response Status
	public static final String SEGMENTS_DETAILS_GET_SUCCESSFULLY="Segments details get successfully.";
	public static final String SEGMENTS_DEFINED_SUCCESSFULLY="Segments defined successfully.";
	public static final String SEGMENTS_DETAILS_UPDATE_SUCCESSFULLY="Segments updated successfully.";
	//User Response Status
	public static final String USERNAME_ALREADY_EXISTS = "Email Already Exists";
	public static final String USERS_IMPORTED_SUCCESSFULLY="Users imported successfully.";
	public static final String USERS_DETAILS_GET_SUCCESSFULLY="Users details get successfully.";
	public static final String USERS_DETAILS_EDITED_SUCCESSFULLY="User details edited successfully.";
	public static final String USERS_CREATED_SUCCESSFULLY="User created successfully.";
	public static final String USERS_DE_ACTIVATED_SUCCESSFULLY="deactivated successfully. The user will no longer be able to login";
	public static final String USERS_ACTIVATED_SUCCESSFULLY="has been activated successfully.";
	public static final String USERS_PASSWORD_RESET_SUCCESSFULLY="Password reset successfully.";
	//Program Response Status
	public static final String PROGRAM_LIST_UPLOADED_SUCCESSFULLY="Program List Uploaded Successfully.";	
	public static final String PROGRAM__CBS_DATA_IMPORTED_SUCCESSFULLY="Program CBS data imported successfully.";	
	//Project Response Status
	public static final String PROJECT_LIST_UPLOADED_SUCCESSFULLY="Project List Uploaded Successfully.";
	public static final String PROJECT__CBS_DATA_IMPORTED_SUCCESSFULLY="Project CBS data imported successfully.";
	//LaborRate Response Status
	public static final String LABOR_RATE_IMPORTED_SUCCESSFULLY="Labor RateS imported successfully.";
	//to save picture
	public static final String PICTURE_SAVE_SUCCESSFULLY= "Picture Save successfully.";
	public static final String FAIL_TO_SAVE_PICTURE ="Picture Save Failure";
	public static final String failtouploadfile="Fail to upload file";
	//to save all LOG into AdfLogController
	public static final Logger LOG = LogManager.getLogger(LoginController.class);
	
	
	
}
