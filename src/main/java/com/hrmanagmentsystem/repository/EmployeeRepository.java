package com.hrmanagmentsystem.repository;

import org.springframework.data.repository.CrudRepository;

import com.hrmanagmentsystem.bean.EmployeeDetails;

public interface EmployeeRepository extends CrudRepository<EmployeeDetails, String>{

}
